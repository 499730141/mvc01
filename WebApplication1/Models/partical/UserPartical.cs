﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApplication1.Models.partical;

namespace WebApplication1.Models
{
        [MetadataType(typeof(UserMetadata))]
        public partial class User
        {
        }

        public class UserMetadata
        {
            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("姓名")]
            [StringLength(5)] 
            public string NAME { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("信箱")]
            [EmailAddress]
            public string E_MAIL { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("密碼")]
            public string PASSWORD { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("生日")]
            public System.DateTime BIRTHDAY { get; set; }

            [Required(ErrorMessage = "必填欄位")]
            [DisplayName("性別")]
            public int SEX { get; set; }
        } 

    }
